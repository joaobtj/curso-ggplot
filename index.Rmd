--- 
title: "Gráficos com o `ggplot2` para pesquisadores"
author: "João B. Tolentino Jr."
date: "`r Sys.Date()`"
description: "Oferta para o Programa de Formação Continuada (PROFOR) da Universidade Federal de Santa Catarina, Edital 004/2021/PROGRAD - Estatística para professores(as) e pesquisadores(as)."
cover-image: image/cover.jpg
apple-touch-icon: image/cover.jpg
favicon: image/favicon.ico
url: 'https\://curso-ggplot.tolentino.pro.br'
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
---

# Prefácio {-}



**Este material está em preparação.**

Oferta para o Programa de Formação Continuada (PROFOR) da Universidade Federal de Santa Catarina, Edital 004/2021/PROGRAD - Estatística para professores(as) e pesquisadores(as).

```{r echo=FALSE, out.width=300}

knitr::include_graphics("image/ufsc.png")

```


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licença Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Este trabalho está licenciado com uma Licença <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons - Atribuição-CompartilhaIgual 4.0 Internacional</a>.
